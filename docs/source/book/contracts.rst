============================================
Enforcing Patters and Interfaces - Contracts
============================================

Contracts exist to enforce that how an interface is being used or extended.
Having contracts allows for the integrety of the interface to be maintained
despite being extended by third parties.

Having contracts allows for the explicit exposure of specific
