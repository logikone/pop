# -*- coding: utf-8 -*-

import collections.abc as abc
import inspect
import pop.exc
import pop.contract as contract
import pop.hub
import pop.mods.pop.data as data
import pytest


@pytest.fixture(scope="function")
def hub():
    hub = pop.hub.Hub()
    yield hub


class TestImmutableNamespacedMap:
    def test_init(self, hub):
        """
        Verify that an init dict is loaded into the namespace
        """
        init_dict = {"a": 2, "b": 4}
        inm = hub.pop.data.imap(init_dict)
        assert inm == init_dict

    def test_init_dict(self, hub):
        """
        Verify that a dict is converted into a mutable mapping namespace from self.update
        """
        init_dict = {"k": {}}
        inm = hub.pop.data.imap(init_dict)
        assert inm == init_dict
        assert isinstance(inm["k"], abc.MutableMapping)

    def test_setitem(self, hub):
        inm = hub.pop.data.imap({})
        with pytest.raises(TypeError):
            inm["k"] = 1

    def test_delitem(self, hub):
        inm = hub.pop.data.imap({})
        with pytest.raises(TypeError):
            del inm["k"]

    def test_getitem(self, hub):
        inm = hub.pop.data.imap({"k": "v"})
        assert inm.get("k") == "v"

    def test_getattr(self, hub):
        init_dict = {"k": "v"}
        inm = hub.pop.data.imap(init_dict)
        assert inm.k == "v"

    def test_getattr_nested(self, hub):
        d = {"d": "val"}
        c = {"c": d}
        b = {"b": c}
        a = {"a": b}
        inm = hub.pop.data.imap(a)

        assert inm.a == b
        assert inm.a.b == c
        assert inm.a.b.c == d
        assert inm.a.b.c.d == "val"

    def test_setattr(self, hub):
        inm = hub.pop.data.imap({})
        with pytest.raises(TypeError):
            inm.k = "value"

    def test_overwrite_store(self, hub):
        inm = hub.pop.data.imap({})
        with pytest.raises(TypeError):
            inm._store = tuple()

    def test_len(self, hub):
        length = 100
        inm = hub.pop.data.imap({f"item_{d}": d for d in range(length)})
        assert len(inm) == length

    def test_str(self, hub):
        init = {"a": {}, "b": 1, "c": False, "d": None}
        inm = hub.pop.data.imap(init)
        assert str(init) == str(inm)


class TestMutableNamespacedMap:
    def test_init(self, hub):
        """
        Verify that an init dict is loaded into the namespace
        """
        init_dict = {"1": 2, "3": 4}
        mnm = hub.pop.data.map(init_dict)
        assert mnm == init_dict

    def test_init_dict(self, hub):
        """
        Verify that a dict is converted into a mutable mapping namespace from self.update
        """
        init_dict = {"k": {}}
        mnm = hub.pop.data.map(init_dict)
        assert mnm == init_dict
        assert isinstance(mnm["k"], abc.MutableMapping)

    def test_setitem(self, hub):
        mnm = hub.pop.data.map()
        mnm["k"] = 1
        assert mnm._store["k"] == 1

    def test_setitem_nested_dict(self, hub):
        mnm = hub.pop.data.map()
        value = {"a": {"b": {"c": {}}}}
        mnm["k"] = value
        assert mnm._store["k"] == value
        assert isinstance(mnm._store["k"], abc.MutableMapping)
        assert isinstance(mnm._store["k"]["a"], abc.MutableMapping)
        assert isinstance(mnm._store["k"]["a"]["b"], abc.MutableMapping)
        assert isinstance(mnm._store["k"]["a"]["b"]["c"], abc.MutableMapping)

    def test_delitem(self, hub):
        mnm = hub.pop.data.map()
        mnm["k"] = "value"
        del mnm["k"]
        assert mnm == {}

    def test_getitem(self, hub):
        mnm = hub.pop.data.map({"k": "v"})
        assert mnm.get("k") == "v"

    def test_getattr(self, hub):
        init_dict = {"k": "v"}
        mnm = hub.pop.data.map(init_dict)
        assert mnm.k == "v"

    def test_getattr_nested(self, hub):
        d = {"d": {}}
        c = {"c": d}
        b = {"b": c}
        a = {"a": b}
        mnm = hub.pop.data.map(a)

        assert isinstance(mnm.a, abc.MutableMapping)
        assert mnm.a == b
        assert isinstance(mnm.a.b, abc.MutableMapping)
        assert mnm.a.b == c
        assert isinstance(mnm.a.b.c, abc.MutableMapping)
        assert mnm.a.b.c == d
        assert isinstance(mnm.a.b.c.d, abc.MutableMapping)
        assert mnm.a.b.c.d == {}

    def test_setattr(self, hub):
        mnm = hub.pop.data.map()
        mnm.key = "value"
        assert mnm._store["key"] == "value"

    def test_setattr_create_nest(self, hub):
        """
        verify that nested namespace values that have never been set get created when they are accessed
        """
        mnm = hub.pop.data.map()
        mnm.a.b.c = 1

        assert isinstance(mnm.a, abc.MutableMapping)
        assert "b" in mnm.a
        assert isinstance(mnm.a.b, abc.MutableMapping)
        assert "c" in mnm.a.b
        assert mnm.a.b.c == 1

    def test_len(self, hub):
        length = 100
        mnm = hub.pop.data.map({str(d): d for d in range(length)})
        assert len(mnm) == len(mnm._store) == length

    def test_str(self, hub):
        mnm = hub.pop.data.map(
            {"a": {}, "b": 1, "c": False, "d": None, "e": lambda: ""}
        )
        assert str(mnm) == str(mnm._store)


class TestDynamicMutableNamespacedMap:
    def test_init(self, hub):
        """
        Verify that an init dict is loaded into the namespace
        """
        init_dict = {"1": 2, "3": 4}
        map_ = hub.pop.data.map(init_dict)
        assert map_ == init_dict

    def test_init_dict(self, hub):
        """
        Verify that a dict is converted into a mutable mnmping namespace from self.update
        """
        init_dict = {"k": "v"}
        map_ = hub.pop.data.map(init_dict)
        assert map_ == init_dict

    def test_setitem(self, hub):
        map_ = hub.pop.data.map()
        with pytest.raises(
            AttributeError, match="Cannot store values beginning with '_'"
        ):
            map_._foo = True
        with pytest.raises(
            AttributeError, match="Cannot store values beginning with '_'"
        ):
            map_["_bar"] = True

    def test_setitem_nested_dict(self, hub):
        pass  # This is an integration test

    def test_delattr(self):
        map_ = data.MAP()
        map_.foo = True
        delattr(map_, "foo")
        assert "foo" not in map_._store

    def test_delitem(self, hub):
        map_ = data.MAP()
        map_["foo"] = True
        del map_["foo"]
        assert "foo" not in map_._store

    def test_get(self):
        map_ = data.MAP()

        assert map_.get("foo") is None
        assert map_.get("foo", False) is False

        map_.foo = True
        assert map_.get("foo") is True

    def test_getitem(self, hub):
        map_ = hub.pop.data.map({"k": "v"})
        assert map_.get("k") == "v"

    def test_getattr(self, hub):
        init_dict = {"k": "v"}
        map_ = hub.pop.data.map(init_dict)
        assert map_.k == "v"

        with pytest.raises(
            AttributeError,
            match=f"'{map_.__class__.__name__}' object has no attribute '_blah'",
        ):
            map_._blah

    def test_getattr_nested(self, hub):
        d = {"d": {}}
        c = {"c": d}
        b = {"b": c}
        a = {"a": b}
        map_ = hub.pop.data.map(a)

        assert isinstance(map_.a, map_.__class__)
        assert map_.a == b
        assert isinstance(map_.a.b, map_.__class__)
        assert map_.a.b == c
        assert isinstance(map_.a.b.c, map_.__class__)
        assert map_.a.b.c == d
        assert isinstance(map_.a.b.c.d, map_.__class__)
        assert map_.a.b.c.d == {}

    def test_setattr(self, hub):
        pass  # This is an integration test

    def test_setattr_create_nest(self, hub):
        pass  # This is an integration test

    def test_len(self, hub):
        length = 100
        map_ = hub.pop.data.map({str(d): d for d in range(length)})
        assert len(map_) == len(map_._store) == length

    def test_str(self, hub):
        map_ = hub.pop.data.map(
            {"a": {}, "b": 1, "c": False, "d": None, "e": lambda: ""}
        )
        assert str(map_) == str(map_._store)

    def test_get_caller(self, hub):
        pass  # This is an integration test

    @pytest.mark.asyncio
    async def test_refresh(self, hub):
        pass  # This is an integration test


class TestUninitializedValue:
    def test_access_error(self):
        dwrite = data.UninitializedValue(["dwrite"], None)
        with pytest.raises(
            Exception, match="Access of uninitialized value 'dwrite'"
        ) as e:
            str(dwrite)

    def test_path(self):
        dwrite = data.UninitializedValue(["dwrite"], None)

        dwrite_bar = dwrite.bar

        assert isinstance(dwrite_bar, data.UninitializedValue)
        assert dwrite.bar.__dict__["_path"] == ["dwrite", "bar"]

        dwrite_bar_baz = dwrite["bar"].baz
        assert isinstance(dwrite_bar_baz, data.UninitializedValue)
        assert dwrite.bar["baz"].__dict__["_path"] == ["dwrite", "bar", "baz"]

    def test_get(self):
        dwrite = data.UninitializedValue(["dwrite"], None)
        assert dwrite.get("foo") is None
        assert dwrite.get("foo", True) is True

    def test_del(self):
        dwrite = data.UninitializedValue(["dwrite"], None)
        del dwrite["foo"]
        delattr(dwrite, "foo")

    def test_create_on_set(self, hub):
        map_ = hub.pop.data.map()
        map_.foo.bar.baz = True
        assert map_.foo.bar.baz is True

        map_.o.bar["baz"] = True
        assert map_.o.bar.baz is True


class TestWriteLockMAP:
    def test_setitem(self, hub):
        g = hub.pop.data.omap()

        def assign(h):
            g.i1 = "val1"
            g.i2 = "val2"

        cassign = contract.Contracted(None, None, assign, None, "assign")
        cassign()

        i1 = g._store["i1"]
        assert i1.val == "val1"
        i2 = g._store["i2"]
        assert i2.val == "val2"

        assert i1.owner == cassign
        assert i2.owner == cassign

    def test_setitem_nested(self, hub):
        g = hub.pop.data.omap()

        def assign(h):
            g.a.b1 = "item"
            g.w.x = {"y": {"z": {}}}

        cassign = contract.Contracted(None, None, assign, None, "assign")
        cassign()

        assert g.a.b1 == "item", cassign
        node = g
        for i in ("w", "x", "y", "z"):
            assert isinstance(node, g.__class__)
            node = node[i]

    def test_writelock(self, hub):
        g = hub.pop.data.omap()

        def assign1(h):
            g.lineno = inspect.currentframe().f_lineno

        cassign1 = contract.Contracted(None, None, assign1, None, "assign1")
        cassign1()

        with pytest.raises(
            Exception,
            match=f"'lineno' was previously assigned by 'assign1' (.*.py:{g.lineno})",
        ):
            g.lineno = False

        def assign2(h):
            g.lineno = False

        cassign2 = contract.Contracted(None, None, assign2, None, "assign2")

        assert g._store["lineno"].owner is cassign1
        with pytest.raises(
            Exception,
            match=f"'lineno' was previously assigned by 'assign1' (.*.py:{g.lineno})",
        ):
            cassign2()

        assert isinstance(g.lineno, int)

    def test_nested_assignment(self, hub):
        g = hub.pop.data.omap()

        def nest_assign():
            g.foo = True

        def assign(h):
            g.report_line = inspect.currentframe().f_lineno + 1
            nest_assign()

        cassign = contract.Contracted(None, None, assign, None, "assign")
        cassign()

        assert g._store["report_line"].owner is cassign
        with pytest.raises(
            Exception,
            match=f"'foo' was previously assigned by 'assign' (.*.py:{g.report_line})",
        ):
            g.foo = False

        assert g.foo is True

    def test_doesntexist(self, hub):
        g = hub.pop.data.omap()

        with pytest.raises(
            Exception, match="Access of uninitialized value 'doesntexist'"
        ):
            str(g.doesntexist)

    def test_str(self, hub):
        d = {"a": {"b": {"c": 1}}}
        g = hub.pop.data.omap(d)

        assert str(g) == str(d)
        assert g == d
