import pop.hub
import pop.exc
import pytest


@pytest.fixture(scope="function")
def hub():
    hub = pop.hub.Hub()
    hub.pop.sub.add("tests.integration.data.mods")
    yield hub


class TestMutableNamespacedMap:
    def test_setitem(self, hub):
        hub.mods.map.collect_setitem()
        # Verify that the value was set
        assert hub.mods.map.MAP["item"] == "value"
        assert hub.mods.map.MAP.item == "value"

    def test_setitem_nested_dict(self, hub):
        hub.mods.map.collect_setitem_nested()
        map_ = hub.mods.map.MAP

        # Verify that the values were set
        assert map_.item == {"a": {"b": {}}}

        # Verify that the dictionaries were all transformed into MAPs
        assert isinstance(map_.item, map_.__class__)
        assert isinstance(map_["item"]["a"], map_.__class__)
        assert isinstance(map_["item"]["a"]["b"], map_.__class__)

        # Verify parents are correctly assigned
        assert map_["item"]["a"]._parent is map_["item"]
        assert map_["item"]["a"]["b"]._parent is map_["item"]["a"]

    def test_delitem(self, hub):
        hub.mods.map.collect_setitem()
        del hub.mods.map.MAP["item"]
        assert "item" not in hub.mods.map.MAP._store

    def test_setattr(self, hub):
        hub.mods.map.collect_setattr()
        # Verify that the value was set
        assert hub.mods.map.MAP["item"] == "value"

    def test_setattr_create_nest(self, hub):
        hub.mods.map.collect_setattr_nested()
        map_ = hub.mods.map.MAP

        # Verify that the dictionaries were all transformed into MAPs
        assert isinstance(map_["item"], map_.__class__)
        assert isinstance(map_["item"]["a"], map_.__class__)
        assert isinstance(map_["item"]["a"]["b"], map_.__class__,)

        assert map_["item"]["a"]["b"]._parent is map_["item"]["a"]
